use clap::{App, Arg};
use rand::Rng;
use librust::{
    fs::File,
    thread::sleep,
    io::{self, BufReader, BufRead}
};
use std::time::Duration;
use wasm_bindgen::prelude::*;
use futures::StreamExt;

mod cat;

#[wasm_bindgen]
pub async fn start() {
    let mut filename: String = "".to_string();
    let mut c = parse_cli_args(&mut filename).await;
    let stdin = io::stdin(); // For lifetime reasons

    if filename == "" {
        let mut line = String::new();
        let mut lock = stdin.lock();

        while let Ok(count) = lock.read_line(&mut line).await {
            if count == 0 {
                break;
            }

            cat::print_with_lolcat(line.clone(), &mut c).await;
            if c.dialup_mode {
                let stall = Duration::from_millis(rand::thread_rng().gen_range(30, 200));
                sleep(stall).await;
            }
        }
    } else if lolcat_file(&filename, &mut c).await.is_err() {
        println!("Error opening file {}.", filename)
    }
}

async fn lolcat_file(filename: &str, c: &mut cat::Control) -> Result<(), io::Error> {
    let f = File::open(filename).await?;
    let file = BufReader::new(&f);
    let mut lines = file.lines();
    while let Some(line) = lines.next().await {
        cat::print_with_lolcat(line.unwrap(), c).await;

        if c.dialup_mode {
            let stall = Duration::from_millis(rand::thread_rng().gen_range(30, 700));
            sleep(stall).await;
        }
    }
    Ok(())
}

async fn parse_cli_args(filename: &mut String) -> cat::Control {
    let matches = App::new("lolcat")
        .version("1.0.1")
        .author("Umang Raghuvanshi <u@umangis.me>")
        .about("The good ol' lolcat, now with fearless concurrency.")
        .arg(
            Arg::new("seed")
                .short('s')
                .long("seed")
                .about("A seed for your lolcat. Setting this to 0 randomizes the seed.")
                .takes_value(true),
        )
        .arg(
            Arg::new("spread")
                .short('S')
                .long("spread")
                .about("How much should we spread dem colors? Defaults to 3.0")
                .takes_value(true),
        )
        .arg(
            Arg::new("frequency")
                .short('f')
                .long("frequency")
                .about("Frequency - used in our math. Defaults to 0.1")
                .takes_value(true),
        )
        .arg(
            Arg::new("background")
                .short('B')
                .long("bg")
                .about("Background mode - If selected the background will be rainbow. Default false")
                .takes_value(false),
        )
        .arg(
            Arg::new("dialup")
                .short('D')
                .long("dialup")
                .about("Dialup mode - Simulate dialup connection")
                .takes_value(false),
        )
        .arg(
            Arg::new("filename")
                .about("Lolcat this file. Reads from STDIN if missing")
                .takes_value(true)
                .index(1),
        )
        .get_matches().await;

    let seed = matches.value_of("seed").unwrap_or("0.0");
    let spread = matches.value_of("spread").unwrap_or("3.0");
    let frequency = matches.value_of("frequency").unwrap_or("0.1");
    let background = matches.is_present("background");
    let dialup = matches.is_present("dialup");

    *filename = matches.value_of("filename").unwrap_or("").to_string();

    let mut seed: f64 = seed.parse().unwrap();
    let spread: f64 = spread.parse().unwrap();
    let frequency: f64 = frequency.parse().unwrap();

    if seed == 0.0 {
        seed = rand::random::<f64>() * 10e9;
    }

    cat::Control {
        seed,
        spread,
        frequency,
        background_mode: background,
        dialup_mode: dialup,
    }
}
